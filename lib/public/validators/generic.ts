import { GenericValidator } from '../../public/types'

export const generic: GenericValidator = (validator) => (config) => ({
  ...config,
  ...validator
})
