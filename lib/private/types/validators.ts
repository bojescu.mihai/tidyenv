import { Config } from '../../public/types'
import { Spec } from './spec'

export type Validator<T> = {
  type: string;
  validator: (value: string | undefined) => boolean;
  converter: (value: string) => T;
};

export type ParticularValidator<T> = (config?: Config<T>) => Spec<T>;
