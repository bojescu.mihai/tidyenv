import { errors } from '../../private/errors'
import { Reducer } from '../../private/types'

export const reducer: Reducer = (from, values) => (acc, key) => {
  const validator = values[key]
  const value = from[key as string]

  if (!validator) {
    throw new Error(errors.NO_VALIDATOR(key as string))
  }

  if (value === undefined) {
    if (!validator.default) {
      throw new Error(errors.NO_OPTIONS(key as string, validator.type))
    }

    acc[key] = validator.default
  } else {
    if (!validator.validator(value)) {
      throw new Error(errors.NOT_VALID(key as string, validator.type))
    }

    acc[key] = validator.converter(value)
  }

  return acc
}
