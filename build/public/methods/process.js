"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.process = void 0;
const methods_1 = require("../../private/methods");
const process = (from, values) => {
    const entries = methods_1.keysOf(values);
    const unfrozenResult = entries.reduce(methods_1.reducer(from, values), {});
    return Object.freeze(unfrozenResult);
};
exports.process = process;
//# sourceMappingURL=process.js.map