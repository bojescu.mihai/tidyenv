import { Specs } from '../../private/types';
export declare const process: <T extends Record<string, unknown>>(from: Record<string, string | undefined>, values: Specs<T>) => Readonly<T>;
//# sourceMappingURL=process.d.ts.map