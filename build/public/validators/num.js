"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.num = void 0;
const generic_1 = require("./generic");
const num = (config) => generic_1.generic({
    type: 'number',
    validator: (value) => value !== undefined && !Number.isNaN(+value),
    converter: (value) => Number(value)
})(config);
exports.num = num;
//# sourceMappingURL=num.js.map