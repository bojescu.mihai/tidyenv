"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.bool = void 0;
const generic_1 = require("./generic");
const bool = (config) => generic_1.generic({
    type: 'boolean',
    validator: (value) => (typeof value === 'string' && ['true', 'false', '1', '0', ''].includes(value.toLocaleLowerCase())) ||
        (value !== undefined && !Number.isNaN(+value) && (+value === 0 || +value === 1)),
    converter: (value) => ((['true', '1'].includes(value.toLocaleLowerCase()) || +value === 1) && true) ||
        ((['false', '0', ''].includes(value.toLocaleLowerCase()) || +value === 0) && false)
})(config);
exports.bool = bool;
//# sourceMappingURL=bool.js.map