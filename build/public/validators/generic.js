"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.generic = void 0;
const generic = (validator) => (config) => ({
    ...config,
    ...validator
});
exports.generic = generic;
//# sourceMappingURL=generic.js.map