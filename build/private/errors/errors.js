"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NOT_VALID = exports.NO_OPTIONS = exports.NO_VALIDATOR = void 0;
const NO_VALIDATOR = (entry) => `Error while reading the '${entry}' value': Validator not valid.`;
exports.NO_VALIDATOR = NO_VALIDATOR;
const NO_OPTIONS = (entry, type) => `Error while reading the '${entry}' value with type '${type}': No default or value provided.`;
exports.NO_OPTIONS = NO_OPTIONS;
const NOT_VALID = (entry, type) => `Error while reading the '${entry}' value with type '${type}': Types don't match.`;
exports.NOT_VALID = NOT_VALID;
//# sourceMappingURL=errors.js.map