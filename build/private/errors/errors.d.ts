export declare const NO_VALIDATOR: (entry: string) => string;
export declare const NO_OPTIONS: (entry: string, type: string) => string;
export declare const NOT_VALID: (entry: string, type: string) => string;
//# sourceMappingURL=errors.d.ts.map