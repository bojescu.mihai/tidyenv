import { Config } from '../../public/types';
import { Validator } from './validators';
export declare type Spec<K> = Config<K> & Validator<K>;
export declare type Specs<T> = {
    [K in keyof T]: Spec<T[K]>;
};
//# sourceMappingURL=spec.d.ts.map