import { Specs } from '../../private/types';
export declare type KeysOf = <T extends Record<string, unknown>>(obj: T) => (keyof T)[];
export declare type Reducer = <T>(from: Record<string, string | undefined>, values: Specs<T>) => (acc: T, key: keyof T) => T;
export declare type Tidy = <T>(from: Record<string, string | undefined>, values: Specs<T>) => Readonly<T>;
//# sourceMappingURL=methods.d.ts.map