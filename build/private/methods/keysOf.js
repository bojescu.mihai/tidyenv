"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.keysOf = void 0;
const keysOf = (obj) => Object.keys(obj);
exports.keysOf = keysOf;
//# sourceMappingURL=keysOf.js.map